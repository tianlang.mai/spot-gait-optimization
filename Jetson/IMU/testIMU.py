from imu import IMU

imu = IMU()

while True:
    gyroX, gyroY, gyroZ = imu.getGyro()
    accX, accY, accZ = imu.getAcceleration()
    roll, pitch = imu.calculateRollPitch()
    temperature = imu.getTemperature()
    print(f'Temperatur: {temperature}')
    print(f'GyroX: {gyroX}, GyroY: {gyroY}, GyroZ: {gyroZ}')
    print(f'AccX: {accX}, AccY: {accY}, AccZ: {accZ}')
    print(f'Roll: {roll}')
    print(f'Pitch: {pitch}')