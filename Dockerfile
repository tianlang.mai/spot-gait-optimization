FROM ubuntu:20.04
USER root

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && \
    apt-get -y -qq install apt-utils sudo 
RUN apt-get update && \
    apt-get -y -qq install lsb-release wget gpg 
RUN apt-get update && \
    apt-get -y -qq install git python3-pip curl nano

RUN mkdir -p /home/developer && \
    cd /home/developer && \
    git clone https://git.rwth-aachen.de/tianlang.mai/spot-gait-optimization.git && \ 
    apt-get update && apt-get upgrade -y

WORKDIR /home/developer/spot-gait-optimization

RUN cd /home/developer/spot-gait-optimization && \
    python3 -m pip install -r requirements.txt
