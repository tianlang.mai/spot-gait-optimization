#!/usr/bin/env python3

import gym
from gym.envs.registration import register
#from Simulation import spot_gym_env

if __name__ == '__main__':
    register(
        id="SpotMicroEnv-v0",
        entry_point='spot_gym_env:spotGymEnv',
        max_episode_steps=1000,
    )

    env = gym.make('SpotMicroEnv-v0')

    for _ in range(10):
        state, done = env.reset(), False
        while not done:
            action = env.action_space.sample()
            state, reward, done, _ = env.step(action)
    
    env.close()
