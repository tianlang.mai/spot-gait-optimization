from os import system, name 
import sys
import timeit
sys.path.append(".")
sys.path.append("../Previous")
sys.path.append("../IMU")
sys.path.append("../../spotRP/Simulation")
sys.path.append("../../Bezier-ARS")

import matplotlib.animation as animation
import numpy as np
import time
import math
import datetime as dt
import copy

import Kinematics.kinematics as kn
import spotmicroai_medit as spotmicroai
import servo_controller_medit as servo_controller
from Kinematics.kinematicMotion import KinematicMotion

from SpotKinematics import SpotModel
from ars_lib.ars import Normalizer, Policy

from GaitGenerator.Bezier import BezierGait
from OpenLoopSM.SpotOL import BezierStepper

from imu import IMU

# Params for TG
CD_SCALE = 0.05
SLV_SCALE = 0.05
RESIDUALS_SCALE = 0.03
Z_SCALE = 0.05

rtime=time.time()

def reset():
    global rtime
    rtime=time.time()    

robot=spotmicroai.Robot(False,False,reset)
controller = servo_controller.Controllers()

spurWidth=robot.W/2+20
stepLength=0
stepHeight=72
# Initial End point X Value for Front legs 
iXf=120

#Leg positions (FR,FL,BR,BL)
Lp = np.array([[iXf, -100, spurWidth, 1], [iXf, -100, -spurWidth, 1],[-50, -100, spurWidth, 1], [-50, -100, -spurWidth, 1]])

motion=KinematicMotion(Lp)

spot = SpotModel()
# Initialize Normalizer
normalizer = Normalizer(12)
# Initialize Policy
policy = Policy(12, 12)
#policy.load('agent_file')



contacts = [0,0,0,0]

smach = BezierStepper(dt=0.01)
TGP = BezierGait(dt=0.01)

imu = IMU()

def main():
    T_bf = copy.deepcopy(spot.WorldToFoot)
    T_b0 = copy.deepcopy(spot.WorldToFoot)
    timesteps = 0
    jointAngles = []
    while True:
        pos, orn, StepLength, LateralFraction, YawRate, StepVelocity, ClearanceHeight, PenetrationDepth = smach.StateMachine()
        # Read UPDATED state based on controls and phase
        state = getState()
        normalizer.observe(state)
        # Don't normalize contacts
        state = normalizer.normalize(state)
        action = policy.evaluate(state)
        action = np.tanh(action)
        ClearanceHeight += action[0] * CD_SCALE
        # CLIP EVERYTHING
        StepLength = np.clip(StepLength, smach.StepLength_LIMITS[0],
                                smach.StepLength_LIMITS[1])
        StepVelocity = np.clip(StepVelocity,
                                smach.StepVelocity_LIMITS[0],
                                smach.StepVelocity_LIMITS[1])
        LateralFraction = np.clip(LateralFraction,
                                    smach.LateralFraction_LIMITS[0],
                                    smach.LateralFraction_LIMITS[1])
        YawRate = np.clip(YawRate, smach.YawRate_LIMITS[0],
                            smach.YawRate_LIMITS[1])
        ClearanceHeight = np.clip(ClearanceHeight,
                                    smach.ClearanceHeight_LIMITS[0],
                                    smach.ClearanceHeight_LIMITS[1])
        PenetrationDepth = np.clip(PenetrationDepth,
                                    smach.PenetrationDepth_LIMITS[0],
                                    smach.PenetrationDepth_LIMITS[1])
        yaw = ...
        # Get Desired Foot Poses
        if timesteps > 5:
            T_bf = TGP.GenerateTrajectory(StepLength, LateralFraction,
                                                YawRate, StepVelocity, T_b0,
                                                T_bf, ClearanceHeight,
                                                PenetrationDepth, contacts)
        else:
            T_bf = TGP.GenerateTrajectory(0.0, 0.0, 0.0, 0.1, T_b0,
                                                T_bf, ClearanceHeight,
                                                PenetrationDepth, contacts)
            action[:] = 0.0
        
        action[2:] *= RESIDUALS_SCALE

        # Add DELTA to XYZ Foot Poses
        T_bf_copy = copy.deepcopy(T_bf)
        T_bf_copy["FL"][:3, 3] += action[2:5]
        T_bf_copy["FR"][:3, 3] += action[5:8]
        T_bf_copy["BL"][:3, 3] += action[8:11]
        T_bf_copy["BR"][:3, 3] += action[11:14]
        # Adjust Height!
        pos[2] += abs(action[1]) * Z_SCALE

        jointAngles = spot.IK(orn, pos, T_bf_copy)
        controller.servoRotate(jointAngles)
        time.sleep(0.01)
        timesteps += 1
        

def getState():
    lin_acc = imu.getCorrectedAcceleration()
    ang_twist = imu.getGyro()
    roll, pitch = imu.calculateRollPitch()
    observation = []
    observation.append(roll)
    observation.append(pitch)
    observation.extend(list(ang_twist))
    observation.extend(list(lin_acc))
    observation.extend(TGP.Phases)

    return observation


if __name__ == "__main__":
    main()