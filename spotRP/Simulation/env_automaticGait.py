#!/usr/bin/env python3

import gym
from gym.envs.registration import register
#from Simulation import spot_gym_env

import sys
sys.path.append("../../Jetson/Previous")
sys.path.append("../../Bezier-ARS")
from Kinematics.kinematicMotion import KinematicMotion, TrottingGait
import spotmicroai_medit as spotmicroai

from spot_bezier_env import spotBezierEnv

import matplotlib.animation as animation
import numpy as np
import time
import math
import datetime as dt

from SpotKinematics import SpotModel

from multiprocessing import Process
from multiprocess_kb import KeyInterrupt

rtime=time.time()

def reset():
    global rtime
    rtime=time.time()  

robot=spotmicroai.Robot(False,False,reset)

# TODO: Needs refactoring
speed1=240
speed2=170
speed3=300

speed1=322
speed2=237
speed3=436

spurWidth=robot.W/2+20
stepLength=0
stepHeight=72

# Initial End point X Value for Front legs 
iXf=120

walk=False



def resetPose():
    # TODO: globals are bad
    global joy_x, joy_z, joy_y, joy_rz
    joy_x, joy_y, joy_z, joy_rz = 128, 128, 128, 128


 #Leg positions (FR,FL,BR,BL)
Lp = np.array([[iXf, -100, spurWidth, 1], [iXf, -100, -spurWidth, 1],
[-50, -100, spurWidth, 1], [-50, -100, -spurWidth, 1]])

motion=KinematicMotion(Lp)
resetPose()

trotting=TrottingGait()

def main(id, command_status):
    env = spotBezierEnv(render=True,
                        on_rack=False,
                        height_field=False,
                        draw_foot_path=False,
                        env_randomizer=None)

    state = env.reset()

    # Set seeds
    env.seed(0)
    np.random.seed(0)

    for _ in range(10):
        state, done = env.reset(), False
        while not done:
            xr = 0.0
            yr = 0.0

            # Reset when robot pose become strange
            # robot.resetBody()
        
            ir=xr/(math.pi/180)
            
            d=time.time()-rtime

            # robot height
            height = 40

            # calculate robot step command from keyboard inputs
            result_dict = command_status.get()
            result_dict['StartStepping'] = False
            if 5 < time.time() - rtime:
                result_dict['StartStepping'] = True
                result_dict['IDstepLength'] = -50
            print(result_dict)
            command_status.put(result_dict)
            
            # wait 3 seconds to start
            
            if result_dict['StartStepping']:
                currentLp = trotting.positions(d-3, result_dict)
                robot.feetPosition(currentLp)
            else:
                robot.feetPosition(Lp)
                
            
            #roll=-xr
            roll=0
            robot.bodyRotation((roll,math.pi/180*((joy_x)-128)/3,-(1/256*joy_y-0.5)))
            bodyX=50+yr*10
            robot.bodyPosition((bodyX, 40+height, -ir))

            # Get current Angles for each motor
            jointAngles = robot.getAngle()
            print(jointAngles)
            
            # First Step doesn't contains jointAngles
            if len(jointAngles):
                env.pass_joint_angles(jointAngles.reshape(-1))
                env.step(jointAngles)
            
            robot.step()
            
            
           
    
    env.close()

if __name__ == '__main__':
     #main()
     try:
         # Keyboard input Process
         KeyInputs = KeyInterrupt()
         KeyProcess = Process(target=KeyInputs.keyInterrupt, args=(1, KeyInputs.key_status, KeyInputs.command_status))
         KeyProcess.start()
 
        # Main Process 
         main(2, KeyInputs.command_status)
        
         print("terminate KeyBoard Input process")
         if KeyProcess.is_alive():
             KeyProcess.terminate()
     except Exception as e:
        print(e)
        print("--------------------------------------------Fehler---------------------------------------------")
     finally:
        print("Done... :)")