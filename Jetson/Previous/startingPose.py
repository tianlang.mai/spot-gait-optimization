from os import system, name 
import sys
sys.path.append(".")

import matplotlib.animation as animation
import numpy as np
import time
import math
import datetime as dt
import keyboard
import random

import Kinematics.kinematics as kn
import spotmicroai_medit as spotmicroai


from multiprocessing import Process
from Kinematics.kinematicMotion import KinematicMotion, TrottingGait

rtime=time.time()

def reset():
    global rtime
    rtime=time.time()    

def resetPose():
    # TODO: globals are bad
    global joy_x, joy_z, joy_y, joy_rz
    joy_x, joy_y, joy_z, joy_rz = 128, 128, 128, 128

robot=spotmicroai.Robot(False,False,reset)

speed1=240
speed2=170
speed3=300

speed1=322
speed2=237
speed3=436

spurWidth=robot.W/2+20
stepLength=0
stepHeight=72

# Initial End point X Value for Front legs 
iXf=120

walk=False

#Leg positions (FR,FL,BR,BL)
Lp = np.array([[iXf, -100, spurWidth, 1], [iXf, -100, -spurWidth, 1],
[-50, -100, spurWidth, 1], [-50, -100, -spurWidth, 1]])

motion=KinematicMotion(Lp)
resetPose()

xr = 0.0
yr = 0.0
ir=xr/(math.pi/180)
height = 40

robot.feetPosition(Lp)

roll=0
robot.bodyRotation((roll,math.pi/180*((joy_x)-128)/3,-(1/256*joy_y-0.5)))
bodyX=50+yr*10
robot.bodyPosition((bodyX, 40+height, -ir))

print(Lp)

# Get current Angles for each motor
jointAngles = robot.getAngle()
print(jointAngles)