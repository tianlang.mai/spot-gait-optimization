import time
import math
import board
import busio
import adafruit_mpu6050

class IMU:
    def __init__(self):
        print('Initializing IMU')
        i2c_bus0=(busio.I2C(board.SCL, board.SDA))
        i2c = board.I2C()  # uses board.SCL and board.SDA
        self.mpu = adafruit_mpu6050.MPU6050(i2c)
        
    def getAcceleration(self):
        acceleration = self.mpu.acceleration;
        return acceleration[0], acceleration[1], acceleration[2]

    def getCorrectedAcceleration(self):
        acceleration = self.mpu.acceleration;
        return acceleration[0], acceleration[1], (acceleration[2]-9.81)
    
    def getGyro(self):
        gyro = self.mpu.gyro
        return gyro[0], gyro[1], gyro[2]
    
    def getTemperature(self):
        return self.mpu.temperature      

    def calculateRollPitch(self):
        accX, accY, accZ = self.getAcceleration()
        pitch = math.atan2(accX, math.sqrt(accY*accY + accZ*accZ))
        roll = -math.atan2(accY, math.sqrt(accX*accX + accZ*accZ))
        return roll, pitch


        